import { test, expect } from "@playwright/test";
import { ProductDetails } from "../models/product_details";
import { LoginPage } from "../pages/login-page";

test("Should launch browser", async ({ page }) => {
  await page.goto("https://www.saucedemo.com/");

  const productDetails: ProductDetails[] = [
    {
      name: "Sauce Labs Backpack",
    },
  ];

  const checkoutInformation: CheckoutInformation = {
    firstName: "Sudarshan",
    lastName: "Srinath",
    postalCode: "123456",
  };

  const successMessage = await LoginPage(page)
    //
    .login("standard_user", "secret_sauce")
    .addProductsTocart(productDetails)
    .goToCheckoutPage()
    .checkout(checkoutInformation);

  expect(successMessage).toEqual("THANK YOU FOR YOUR ORDER");
});
