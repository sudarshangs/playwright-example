export interface ProductDetails {
  name: string;
  description?: string;
  price?: string;
  quantity?: Number;
}
