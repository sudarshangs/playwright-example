interface CheckoutInformation {
  firstName: string;
  lastName: string;
  postalCode: string;
}
