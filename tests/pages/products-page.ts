import { Page } from "@playwright/test";
import { ProductDetails } from "../models/product_details";
import { CheckoutPage } from "./checkout-page";
import { createPageObject } from "./page-object";

export const ProductsPage = (driver: Page) =>
  createPageObject({
    //
    driver: driver,
    selectors: {
      inventoryItem: `xpath=//div[@class='inventory_item'][.//*[text() = 'PRODUCT']]`,
      addToCart: "text=Add to cart",
      cart: "a.shopping_cart_link",
    },

    actions: (page, scheduleCallBack, then, driver) => ({
      then,

      addProductsTocart(products: ProductDetails[]) {
        scheduleCallBack(async () => {
          for (
            let productCounter = 0;
            productCounter < products.length;
            productCounter++
          ) {
            const inventoryItem = await page.waitForSelector("inventoryItem", {
              PRODUCT: products[productCounter].name,
            });
            await page.click("addToCart", undefined, inventoryItem);
          }
        });

        return ProductsPage(driver);
      },

      goToCheckoutPage() {
        scheduleCallBack(async () => {
          await page.click("cart");
        });

        return CheckoutPage(driver);
      },
    }),
  });
