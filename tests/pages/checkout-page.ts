import { Page } from "@playwright/test";
import { createPageObject } from "./page-object";

export const CheckoutPage = (driver: Page) =>
  createPageObject({
    //
    driver: driver,
    selectors: {
      checkout: "button#checkout",
      firstname: `[data-test='firstName']`,
      lastname: `[data-test='lastName']`,
      postalCode: `[data-test='postalCode']`,
      continue: `[data-test='continue']`,
      finish: `[data-test='finish']`,
      successMessage: "h2.complete-header",
    },

    actions: (page, scheduleCallBack, then, driver) => ({
      then,

      checkout(checkoutInformation: CheckoutInformation) {
        return scheduleCallBack(async () => {
          await page.click("checkout");
          await page.waitForSelector("firstname");
          await page.fill("firstname", checkoutInformation.firstName);
          await page.fill("lastname", checkoutInformation.lastName);
          await page.fill("postalCode", checkoutInformation.postalCode);
          await page.click("continue");
          await page.click("finish");
          return page.getText("successMessage");
        });
      },
    }),
  });
