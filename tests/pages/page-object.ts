import { ElementHandle, Page } from "@playwright/test";

export class PageObject<T extends Record<string, string>> {
  constructor(private page: Page, private selectors: T) {}

  async click(
    target: keyof T,
    selectorPlaceHolders?: { [key: string]: string },
    scope?: ElementHandle
  ) {
    await (await this.get(target, selectorPlaceHolders, scope)).click();
  }

  async getText(
    target: keyof T,
    selectorPlaceHolders?: { [key: string]: string },
    scope?: ElementHandle
  ) {
    await (
      await this.get(target, selectorPlaceHolders, scope)
    ).scrollIntoViewIfNeeded();

    return await (
      await this.get(target, selectorPlaceHolders, scope)
    ).textContent();
  }

  async fill(
    target: keyof T,
    text: string,
    selectorPlaceHolders?: { [key: string]: string },
    scope?: ElementHandle
  ) {
    await (await this.get(target, selectorPlaceHolders, scope)).fill(text);
  }

  async waitForSelector(
    target: keyof T,
    selectorPlaceHolders?: { [key: string]: string }
  ) {
    return await this.page.waitForSelector(
      this.buildSelector(target, selectorPlaceHolders)
    );
  }

  async get(
    target: keyof T,
    selectorPlaceHolders?: { [key: string]: string },
    scope?: ElementHandle
  ) {
    const selector = this.buildSelector(target, selectorPlaceHolders);
    return scope !== undefined ? scope.$(selector) : this.page.$(selector);
  }

  buildSelector(
    target: keyof T,
    selectorPlaceHolders?: { [key: string]: string }
  ) {
    let selector = this.selectors[target] as string;
    if (selectorPlaceHolders) {
      Object.keys(selectorPlaceHolders).forEach((key) => {
        selector = selector.replace(key, selectorPlaceHolders[key]);
      });
    }

    return selector;
  }
}

export const createPageObject = <T extends Record<string, string>, K>(
  page: PageObjectTemplate<T, K>
): K => {
  return page.actions(
    new PageObject(page.driver, page.selectors),
    scheduleCallBack,
    then,
    page.driver
  );
};

export interface PageObjectTemplate<T extends Record<string, string>, K> {
  driver: Page;
  selectors: T;
  actions: (
    po: PageObject<T>,
    scheduleCallBack: (cb: () => Promise<any>) => Promise<any>,
    then: (callBack: (arg: Promise<any>) => any) => any,
    page: Page
  ) => K;
}

let task = Promise.resolve();

export const scheduleCallBack = (cb: () => Promise<any>) => {
  return (task = task.then(cb));
};

export const then = (callBack: (arg: Promise<any>) => any) => callBack(task);
