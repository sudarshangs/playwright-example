import { Page } from "@playwright/test";
import { createPageObject } from "./page-object";
import { ProductsPage } from "./products-page";

export const LoginPage = (driver: Page) =>
  createPageObject({
    //
    driver: driver,
    selectors: {
      username: `[data-test='username']`,
      password: `[data-test='password']`,
      loginbutton: `[data-test='login-button']`,
    },

    actions: (page, scheduleCallBack, then, driver) => ({
      then,

      login(username: string, password: string) {
        scheduleCallBack(async () => {
          await page.fill("username", username);
          await page.fill("password", password);
          await page.click("loginbutton");
        });

        return ProductsPage(driver);
      },
    }),
  });
