import { PlaywrightTestConfig } from "@playwright/test";

export const config: PlaywrightTestConfig = {
  use: {
    browserName: "firefox",
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
  },
  workers: 2,
};
